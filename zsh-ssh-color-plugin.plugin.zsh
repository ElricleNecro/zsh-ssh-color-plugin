typeset -gA SSH_HOST_ARRAY

SSH_HOST_ARRAY["pc-fixe"]="\033]11;#221155\007"
SSH_HOST_ARRAY["msi-plum"]="\033]11;#551122\007"
SSH_HOST_ARRAY["pc-plum"]="\033]11;#440044\007"
SSH_HOST_ARRAY["proxima"]="\033]11;#112211\007"

function ssh_color_apply() {
	local host=$(hostname)

	if [[ -n "$host" ]] && [[ -n "$SSH_TTY" ]]
	then
		/bin/echo -e $SSH_HOST_ARRAY["$host"]
	fi
}

function ssh_color_clear() {
	local host=$(hostname)

	if [[ -n "$host" ]]
	then
		reset
	fi
}
